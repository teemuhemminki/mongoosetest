//Update muokkaa opiskelijan opintopisteiden määrää
var Opiskelija = require('./opiskelijaSchema.js');

//Exportataan anonyymi funktio
module.exports = function(opnro, opisteet) {
    /*HUOM: Uskoakseni opintopisteet laskettaisiin mieluummin suoritetuista kursseista.
    Tämä voisi onnistua jonkinlaisella summaamisella ja käyttämällä mongoosen aggregaatio
    frameworkkia */

    //Käytetään findOneAndUpdate ehtoa jolla haetaan opiskelija opnron perusteella ja päivitetään opintopisteet opisteet määrällä.
    Opiskelija.findOneAndUpdate({ opiskelijanumero: opnro }, { opintopisteet: opisteet }, function(err, opiskelija) {
        //Callback funktio viestittää konsoliin että asioita tapahtui
        if (err) {
            throw err;
        }

        console.log('Values updated');

    });
}