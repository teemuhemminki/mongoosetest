//add.js lisää parametrien mukaisen opiskelijan

var Opiskelija = require('./opiskelijaSchema.js');

module.exports = function (opnumero, nimi, sposti, pisteet) {

    //Luodaan modelista uusi opiskelija annetuilla parametreilla
    var newOpiskelija = Opiskelija({
        opiskelijanumero: opnumero,
        nimi: nimi,
        email: sposti,
        opintopisteet: pisteet
    });

    //Tallennetaan opiskelija tietokantaan.
    newOpiskelija.save(function(err) {
        if (err) {
            throw err;
        }

        console.log('User created!');
    });
}