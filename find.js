//Find hakee ja palauttaa opiskelijat, joiden opintopisteet ovat alle 100
var Opiskelija = require('./opiskelijaSchema.js');

//Exportataan anonyymi funktio
module.exports = function() {
    //Käytetään where ehtoa ja haetaan less than (lt) 100 opintopisteet. Sitten toteutetaan callback funktio.
    Opiskelija.where('opintopisteet').lt(100).exec(function(err, opiskelijat) {
        if (err) { //Virheilmoittelu
            throw err;
        }

        //Näytetään opiskelijat konsolissa
        console.log(opiskelijat);
    });
}