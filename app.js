//Haetaan moduulit käyttöön
var mongoose = require('mongoose');
var add = require('./add.js');
var remove = require('./delete.js');
var find = require('./find.js');
var update = require('./update.js');
var addCourse = require('./addcourse.js');
var updateGrade = require('./updategrade.js');

var db = mongoose.connection; //Luodaan referenssi mongoosen connectionille

//Avataan tietokantayhteys ja aletaan työskentelemään.
//Vaihda osoitetta tarpeen mukaan. Osoite pitäisi hakea mieluummin jostain config tiedostosta.
db.openUri('mongodb://localhost:27017/websk2mongoose');
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() { //Ajetaan kerran.
    console.log('Connected safely');

    //Alla toiminnot moduulien käyttöön. Poista tai laita kommentteihin tarpeen mukaan.
    //add('d0001', 'Daniel Darko', 'doubleDee@darkolandia.com', 4);
    //add('d0002', 'Danny Darko', 'doubleDee2@darkolandia.com');
    //add('d0003', 'Daniela Darko');
    //remove('d0001'); //Removet tapahtuvat ennen addeja. Yhtaikaista käyttöä varten pitäisi laittaa jokin odotustoiminto.
    //remove('d0002');
    //remove('d0003');

    //update('d0001', 110);

    //addCourse('d0001', 'Angstausta', 20, 4);
    //addCourse('d0001', 'Masennusta', 14, 3);
    //addCourse('d0002', 'Angstausta', 20, 4);

    updateGrade('d0001', 'Angstausta', 5);

    find();

});