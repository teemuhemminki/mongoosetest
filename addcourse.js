//Lisää opiskelijalle uuden kurssin
var Opiskelija = require('./opiskelijaSchema.js');

//Exportataan anonyymi funktio
module.exports = function(opnro, kNimi, kLaajuus, kArvosana) {
    /*HUOM: Tässä ratkaisussa on heikkoutena se, ettei nimen, laajuuden tai arvosanan olemassaoloa tarkasteta.
    Tämä voitaisiin toteuttaa ainakin if-else lauseilla, tai kikkailemalla alidokuentille oma modeli,
    joka vaatii nämä arvot toimiakseen.*/

    //Käytetään findOneAndUpdate ehtoa jolla haetaan opiskelija opnron perusteella ja päivitetään opintopisteet opisteet määrällä.
    Opiskelija.findOneAndUpdate({ opiskelijanumero: opnro },
        //Pushataan kurssit tietueeseen uusi kurssi.
        { $push: { 'kurssit': { 'kurssinimi': kNimi, 'laajuus': kLaajuus, 'arvosana': kArvosana } } },
        function(err, opiskelija) {
        //Callback funktio viestittää konsoliin että asioita tapahtui
            if (err) {
                throw err;
            }

            console.log('Course added');

        });
}