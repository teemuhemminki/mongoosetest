//Muokkaa opiskelijan kurssin arvosanaa
var Opiskelija = require('./opiskelijaSchema.js');

//Exportataan anonyymi funktio
module.exports = function(opnro, kNimi, uusiArvosana) {

    //Etsitään oikea opiskelija ja kurssi kursseista
    Opiskelija.findOneAndUpdate({ opiskelijanumero: opnro, 'kurssit.kurssinimi': kNimi },
        //Asetetaan arvosana ensimmäiselle saadulle kurssille
        { $set: { 'kurssit.$.arvosana': uusiArvosana } },
        function(err, opiskelija) {
        //Callback funktio viestittää konsoliin että asioita tapahtui
            if (err) {
                throw err;
            }

            console.log('Grade updated');

        });
}