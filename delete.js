//Delete poistaa opiskelijan opiskelijanumeron mukaisesti

var Opiskelija = require('./opiskelijaSchema.js');

module.exports = function(opnro) {
    //Etsitään opnro parametrin mukainen opiskelija ja poistetaan se.
    Opiskelija.findOneAndRemove({ opiskelijanumero: opnro }, function(err, opiskelija) {
        if (err) {
            throw err;
        }

        console.log('User deleted');

    });
}